import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import FreeStaff from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<FreeStaff />, document.getElementById('root'));


serviceWorker.unregister();
