import Axios from 'axios';

const BASE_URL = 'http://freestuff.loc/';

const headers = {};

const instance = Axios.create({
    baseURL: BASE_URL,
    headers: {...headers},
    withCredentials: true // Cookie
});

export const usersAPI = {
    async example() {
        const response = await instance.get();
        return response;
    }
};
