import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { reducer as formReducer } from 'redux-form'
import { AuthReducer } from './Reducers/Auth-Reducer';

import thunk from 'redux-thunk';
import AppReducer from './Reducers/App-Reducer';

const reducers = combineReducers({
    auth: AuthReducer,
    init: AppReducer,
    form: formReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));


export default store;

Object.assign(window, window.store, { ___store___: store });
