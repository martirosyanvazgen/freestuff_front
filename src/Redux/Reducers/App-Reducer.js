import {userSignIn} from './Auth-Reducer';

const INIT_SUCCESS = 'appReducer/INIT_SUCCESS';

const initial = {
    initialized: false
};

const AppReducer = (state = initial, action) => {
    switch (action.type) {
        case INIT_SUCCESS:
            return {
                ...state,
                initialized: true
            };
        default:
            return state;
    }
};

export const initSuccess = () => ({type: INIT_SUCCESS});

export const initializeApp = (cookie) => dispatch => {
    if (cookie) {
        const data = {
            email: 'example',
            password: '00000',
            fullName: cookie
        }
        dispatch(userSignIn(data))
        dispatch(initSuccess())
    } else {
        dispatch(initSuccess())
    }
};

export default AppReducer;
