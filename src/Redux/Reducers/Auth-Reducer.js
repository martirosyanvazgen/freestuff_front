import Cookies from 'js-cookie'

const USER_SIGN_IN = 'auth-Reducer/USER_SIGN_IN';
const USER_SIGN_UP = 'auth-Reducer/USER_SIGN_UP';
const USER_SIGN_OUT = 'auth-Reducer/USER_SIGN_OUT';

const initial = {
    email: null,
    password: null,
    isAuth: false,
    fullName: null
};

export const AuthReducer = (state = initial, action) => {
    switch (action.type) {
        case USER_SIGN_IN:
            return {
                ...state,
                ...action.form,
                fullName: action.form.fullName ? action.form.fullName : 'example',
                isAuth: true
            };
        case USER_SIGN_UP:
            return {
                ...state,
                ...action.form,
                isAuth: true
            };
        case USER_SIGN_OUT:
            return {
                ...state,
                email: null,
                id: null,
                isAuth: false,
                login: null,
                fullName: null
            };
        default:
            return state;
    }
};

// // // // // ACTION CREATORS // // // // // 

const userSignInAC = (form) => ({
    type: USER_SIGN_IN,
    form: {
        email: form.email,
        password: form.password,
        fullName: form.fullName
    }
});

const userSignUpAC = (form) => ({
    type: USER_SIGN_UP,
    form: {
        email: form.email,
        password: form.password,
        fullName: form.firstName + ' ' + form.lastName
    }
});

const userSignOutAC = () => ({ type: USER_SIGN_OUT });

// // // // // THUNKS // // // // // 

export const userSignIn = (formData) => {
    return dispatch => {
        dispatch(userSignInAC(formData));
    }
};

export const userSignUp = (formData) => {
    return dispatch => {
        Cookies.set('userName', formData.firstName + ' ' + formData.lastName, { expires: 7, path: '/' });
        dispatch(userSignUpAC(formData));
    }
};

export const userSignOut = () => {
    return dispatch => {
        Cookies.remove('userName', { path: '/' })
        dispatch(userSignOutAC());
    }
};

export const getAuthUserData = () => dispatch => {
    return dispatch => {

    }
};
