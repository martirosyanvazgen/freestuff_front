import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

export const WithAuthRedirect = (Component) => {

    class RedirectComponent extends React.Component {
        render() {
            if (!this.props.isAuth) {
                alert('Please signIn first');
                return <Redirect to='/login' />
            }
            return (
                <Component {...this.props} />
            )
        }
    }


    return connect(mapStateToPropsForRedirect)(RedirectComponent);
};


const mapStateToPropsForRedirect = (state) => ({ isAuth: state.auth.isAuth });
