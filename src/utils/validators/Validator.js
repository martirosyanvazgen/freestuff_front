export const required = value => {
    if(!value) {
        return 'Form is required';
    } else {
        return undefined;
    }
};

export const minLengthCreator = minLength => value => {
    if (minLength > value.length) {
        return `Min length is ${minLength} symbols`;
    } else {
        return undefined;
    }
};

export const maxLengthCreator = maxLength => value => {
    if (maxLength < value.length) {
        return `Max length is ${maxLength} symbols`;
    } else {
        return undefined;
    }
};
