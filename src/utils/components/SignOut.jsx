import s from './C.module.scss';

import { userSignOut } from '../../Redux/Reducers/Auth-Reducer';

import React from 'react';

export const SignOut = (props) => {
    const logOut = () => {
        props.dispatch(userSignOut());
    };

    return (
        <div>
            <button className={s.btn_sing_out} onClick={logOut}>Sing out</button>
        </div>
    )
}
