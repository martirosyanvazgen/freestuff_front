import { compose } from 'redux';
import { connect, Provider } from 'react-redux';
import { initializeApp } from './Redux/Reducers/App-Reducer';

import { BrowserRouter, withRouter, Route } from 'react-router-dom';
import { Catalog } from './components/Changeable/Catalog/Catalog';
import { Footer } from './components/Static/Footer/Footer';
import { Header } from './components/Static/Header/Header'
import { Introduce } from './components/Changeable/Introduce/Introduce';

import CategoryContainer from './components/Changeable/Category/CategoryContainer';
import Cookies from 'js-cookie'
import Login from './components/Changeable/Login/Login';
import Profile from './components/Changeable/Profile/Profile';
import Register from './components/Changeable/Registration/Registration';
import React from 'react';

import store from './Redux/Store';
import './App.scss';


class App extends React.Component {

  componentDidMount() {
    const value = Cookies.get('userName');
    this.props.initializeApp(value);
  }

  render() {
    if (this.props.init) {
      return (
        <div className='App'>
          <Header userData={{ ...this.props.userData }} />
          <Route path='/login' render={() => <Login dispatch={store.dispatch} />} />
          <Route path='/registration' render={() => <Register dispatch={store.dispatch} />} />
          <Route path='/profile' render={() => <Profile dispatch={store.dispatch} />} />
          <Route path='/category/:slug' render={() => <CategoryContainer dispatch={store.dispatch} />} />
          <Route path='/' exact render={() => <Introduce />} />
          <Route path='/' exact render={() => <Catalog />} />
          <Footer />
        </div>
      );
    } else {
      return <h2>Loading...</h2>
    }

  }
}

function FreeStaff(props) {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <AppContainer />
      </Provider>
    </BrowserRouter>
  );
}

const mapStateToProps = (state) => ({
  userData: state.auth,
  init: state.init.initialized
});

const AppContainer = compose(
  connect(mapStateToProps, { initializeApp }),
  withRouter
)(App);

export default FreeStaff;
