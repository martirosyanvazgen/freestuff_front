import React from 'react'

import img from 'src/assets/Images/search.png';
import s from './Search.module.scss'

export const Search = (props) => {
    return (
        <div className={s.s_main}>
            <div className={s.s_search_img}>
                <img src={img} alt="search_img" />
            </div>
            <div className={s.s_search_inp}>
                <input type="text" placeholder='search' />
            </div>
        </div>
    )
};
