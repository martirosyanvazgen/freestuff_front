import { NavLink } from 'react-router-dom';

import React from 'react';

import s from './HCategories.module.scss';

export const Categories = (props) => {
    return (
        <div className={s.categories_main}>
            {
                props.categories.map((c, i) => {
                    return (
                        <div className={s.cat_item} key={i}>
                            <span className={`${s.cat_name} t_c`}>
                                <NavLink activeClassName={s.active} to={`/category/${c.name}`}>
                                    {c.name}
                                </NavLink>
                            </span>
                        </div>
                    )
                })
            }
        </div>
    )
};
