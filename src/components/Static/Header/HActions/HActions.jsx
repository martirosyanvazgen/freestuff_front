import { NavLink } from 'react-router-dom';

import Arrow from 'src/assets/Images/ArrowDropDown.png'
import React, { useState, useEffect, useRef } from 'react';
import User from 'src/assets/Images/User.png';

import s from './HActions.module.scss';

const HLogin = ({ userData }) => {
    return (
        <div className={s.login}>
            <div className={s.login_text}>
                {
                    userData.isAuth ?
                        <NavLink to='/profile'>
                            <p>{userData.fullName}</p>
                        </NavLink>
                        :
                        <NavLink to='login'>
                            <p className={'t_c'}>Login</p>
                        </NavLink>
                }
            </div>
            <div className={s.login_img}>
                <img src={User} alt="user_IMG" />
            </div>
        </div>
    )
};

const HButton = (props) => {
    return (
        <div className={s.add_button}>
            <button className={'t_c'}> + Add New Product</button>
        </div>
    )
};

const HLang = (props) => {
    const node = useRef();

    const [dropDown, toggleDropDown] = useState(false);
    const [lang, changeLanguage] = useState({ key: 'en', value: 'ENG' });

    const clickOut = e => node.current.contains(e.target) ? '' : toggleDropDown(false);

    const toggle = () => toggleDropDown(!dropDown);

    const changeLan = (lan) => changeLanguage(lan);

    useEffect(() => {
        if (dropDown) {
            document.addEventListener('mousedown', clickOut);
        } else {
            document.removeEventListener('mousedown', clickOut);
        }
        return () => {
            document.removeEventListener('mousedown', clickOut);
        };
    }, [dropDown]);

    return (
        <div ref={node} className={s.languages}>
            <div onClick={toggle}>
                <span>{lang.value}</span>
                <div className={`${s.lan_list} ${dropDown ? s.active : ''}`}>
                    <span onClick={() => changeLan({ key: 'en', value: 'ENG' })} className={`${s.lan_list_item} t_c`}>ENG</span>
                    <span onClick={() => changeLan({ key: 'ru', value: 'RUS' })} className={`${s.lan_list_item} t_c`}>RUS</span>
                    <span onClick={() => changeLan({ key: 'am', value: 'ARM' })} className={`${s.lan_list_item} t_c`}>ARM</span>
                </div>
            </div>
            <div onClick={toggle} className={s.lan_arrow}>
                <img className={dropDown ? s.up : s.down} src={Arrow} alt="" />
            </div>
        </div>
    )
};

class HActions extends React.Component {
    constructor(props) {
        super(props);
        this.lanChange = this.lanChange.bind(this);
    }

    lanChange(e) {
        alert(e.target.value);
    }

    render() {
        console.log()
        return (
            <div className={s.actions_main}>
                <HLogin userData={this.props.userData} />
                <HButton />
                <HLang change={this.lanChange} />
            </div>
        )
    }
};

export default HActions;
