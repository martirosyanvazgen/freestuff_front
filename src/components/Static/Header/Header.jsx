import { NavLink } from 'react-router-dom';
import { Search } from './Search/Search';

import HActions from './HActions/HActions'
import React from 'react';

import logo from 'src/assets/Images/Logo.png';
import s from './Header.module.scss'
import { Categories } from './HCategories/HCategories';

export const Header = ({ userData }) => {
    const arr = Array(10).fill(({ name: 'Products' }));

    return (
        <div className={s.header}>
            <nav className={s.header_main}>
                <div className={s.header_img}>
                    <NavLink to='/'>
                        <img src={logo} alt="logo" />
                    </NavLink>
                </div>
                <div className={s.header_search}>
                    <Search />
                </div>
                <div className={s.header_actions}>
                    <HActions userData={{ ...userData }} />
                </div>
            </nav>
            <div className={s.categories}>
                <Categories categories={arr} />
            </div>
        </div>
    )
}
