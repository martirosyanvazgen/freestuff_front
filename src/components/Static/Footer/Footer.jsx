import React from 'react';

import logo from 'src/assets/Images/FLogo.png';
import facebook from 'src/assets/Images/facebook.png';
import insta from 'src/assets/Images/instagram.png';

import s from './Footer.module.scss'

const FLogo = (props) => {
    return (
        <div className={s.logo}>
            <img src={logo} alt="logoIMG" />
            <h2 className={`${s.logo_title} t_c`}>Lorem ipsum</h2>
        </div>
    )
}

const FInfo = (props) => {
    return (
        <div className={s.info}>
            <div className={s.info_block}>
                <h2 className={s.info_ul_title}>CORPORATE HEADQUARTERS</h2>
                <ul className={s.info_ul}>
                    <li className={s.info_li}>1820 Bonanza Street</li>
                    <li className={s.info_li}>Walnut Creek</li>
                    <li className={s.info_li}>CA 94596</li>
                    <li className={s.info_li}>925.983.2800</li>
                </ul>
                <div className={s.info_img}>
                    <div>
                        <img src={facebook} alt="facebookIMG" />
                    </div>
                    <div>
                        <img src={insta} alt="instagramIMG" />
                    </div>
                </div>
            </div>
            <div className={s.info_block}>
                <h2 className={s.info_ul_title}>INFORMATION</h2>
                <ul className={s.info_ul}>
                    <li className={s.info_li}>Terms & Conditions</li>
                    <li className={s.info_li}>Privacy Policy</li>
                    <li className={s.info_li}>Cookie Policy</li>
                    <li className={s.info_li}>FAQ</li>
                </ul>
            </div>
            <div className={s.info_block}>
                <h2 className={s.info_ul_title}>MY ACCOUNT</h2>
                <ul className={s.info_ul}>
                    <li className={s.info_li}>My Account</li>
                    <li className={s.info_li}>Wish List</li>
                </ul>
            </div>
            <div className={s.info_credits}>
                <p className={`${s.credits} t_c`}>2019 One Planet Ops Inc. All Rights Reserved</p>
            </div>
        </div>
    )
}

export const Footer = (props) => {
    return (
        <div className={s.footer_main}>
            <FLogo />
            <FInfo />
        </div>
    )
};
