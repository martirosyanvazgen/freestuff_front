import React, {useState} from 'react';
import * as s from './FormControl.module.scss';

export const Input = ({input, meta, ...props}) => {
    const hasError = meta.touched && meta.error;

    const [value, changeValue] = useState(input.value);

    const change = (e) => {
        changeValue(e.target.value);
    };

    return (
        <div className={hasError ? `${s.formControl} ${s.error}` : `${s.formControl}`}>
            <div>
                <input {...input} {...props} onChange={change} value={value} />
            </div>
            {hasError && <span>{meta.error}</span>}
        </div>
    )
};

export const Textarea = ({input, meta, ...props}) => {
    return (
        <div>
            <div className={s.formControl}>
                <textarea {...input} {...props}/>
            </div>
            <span>{meta.error}</span>
        </div>
    )
};
