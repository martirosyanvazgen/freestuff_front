import React, { useState, useEffect, useRef } from 'react';

import Arrow from 'src/assets/Images/ArrowDropDown.png';
import s from './CType.module.scss';



export const FilterType = ({ filterType, chooseType }) => {
    const node = useRef();

    const [dropDown, toggleDropDown] = useState(false);
    const [content, changeContent] = useState({ name: filterType.name, value: 'select' });

    const toggle = () => toggleDropDown(!dropDown);

    const changeCon = (con) => changeContent(con);

    const clickOut = e => node.current.contains(e.target) ? '' : toggleDropDown(false);
    useEffect(() => {
        if (dropDown) {
            document.addEventListener('mousedown', clickOut);
        } else {
            document.removeEventListener('mousedown', clickOut);
        }
        return () => {
            document.removeEventListener('mousedown', clickOut);
        };
    }, [dropDown]);

    return (
        <div ref={node} className={s.filter_main}>
            <span className={s.name}>{filterType.name}</span>
            <div onClick={toggle} className={s.chooenName}>
                <span>{content.value}</span>
                <div onClick={toggle} className={s.arrow}>
                    <img className={dropDown ? s.up : s.down} src={Arrow} alt="" />
                </div>
                <div className={`${s.list} ${dropDown ? s.active : ''}`}>
                    {filterType.filter.map(f => <span onClick={() => changeCon({ name: filterType.name, value: f })} className={`${s.list_item} t_c`}>{f}</span>)}
                    {/* <span onClick={() => changeCon({ value: 'ENG' })} className={`${s.list_item} t_c`}>ENG</span> */}
                </div>
            </div>

        </div>
    )
}