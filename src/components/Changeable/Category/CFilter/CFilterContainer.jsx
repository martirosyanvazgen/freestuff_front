import { FilterType } from './CType/CType';

import React from 'react';

import s from './CFilter.module.scss';


class CFilterContainer extends React.Component {
    state = {
        types: [
            {
                name: 'Type',
                filter: [
                    'Jacket',
                    'Outwears',
                    'Jacket+outwear'
                ]
            },
            {
                name: 'Size',
                filter: [
                    'XXL',
                    'XL',
                    'L',
                    'M',
                    'S'
                ]
            },
            {
                name: 'Color',
                filter: [
                    'Green',
                    'yellow',
                    'black',
                    'red',
                    'blue'
                ]
            },
            {
                name: 'Genter',
                filter: [
                    'i dont know',
                    'i dont know x2',
                    'i dont know x3'
                ]
            }
        ],

        choosenType: {
            type: '',
            size: '',
            color: '',
            genter: ''
        }
    };

    constructor(props) {
        super(props);
        this.chooseType.bind(this.chooseType);
    }

    chooseType(filter) {
        this.setState({
            choosenType: { ...this.state.choosenType, filter }
        }, () => console.log(this.state))
    }

    render() {
        return (
            <div className={s.filter_container}>
                {this.state.types.map((t, i) => <FilterType key={i} chooseType={this.chooseType} filterType={t} />)}
            </div>
        )
    }
}

export default CFilterContainer;