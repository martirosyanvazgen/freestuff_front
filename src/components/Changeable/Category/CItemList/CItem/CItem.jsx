import img from 'src/assets/Images/category/facebook.png';
import star from 'src/assets/Images/category/star.png';
import eye from 'src/assets/Images/category/views.png';

import s from './CItem.module.scss';

import React from 'react';




export const CItem = ({
    id,
    name,
    favorite,
    size,
    color,
    user,
    data,
    views,
    addToFavorite
}) => {

    const addToFav = (id, order) => {
        addToFavorite(id, order);
    }

    return (
        <div className={s.item_main}>
            <div className={s.item_img}>
                <img src={img} alt="" />
                <img src={star} alt="" />
            </div>
            <div className={s.item_text}>
                <div className={`${s.item_name} t_c`}>
                    <h2>{name}</h2>
                </div>
                <div className={`${s.item_size_color} t_c`}>
                    <b>Size: {size}</b>
                    <b>Color: {color}</b>
                </div>
                <div className={`${s.item_user} t_c`}>
                    User: <b>{user}</b>
                </div>
                <div className={s.item_additional}>
                    <div className={s.data}>{data}</div>
                    <div className={s.views}>
                        <b>{views}</b>
                        <img src={eye} alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}
