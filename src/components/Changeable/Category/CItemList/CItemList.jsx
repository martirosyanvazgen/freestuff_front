import { CItem } from './CItem/CItem';

import React from 'react';

import s from './CItemList.module.scss';

class ItemListContainer extends React.Component {
    state = {
        items: [
            {
                id: 1,
                name: 'Velour Girls\' Down Jacket JR',
                favorite: false,
                size: 'XXL',
                color: 'Navy',
                user: 'Lana Dolores',
                data: '14.11.2001',
                views: 142
            }
        ]
    };


    constructor(props) {
        super(props);
        this.addToFavorite.bind(this.addToFavorite);
    }

    setState({ pervState, props }) {

    }

    addToFavorite(id, order) {
        console.log(id, order);
    }

    render() {
        return (
            <div className={s.items_main}>
                <div className={s.item}>
                    {
                        this.state.items.map(it => <CItem
                            id={it.id}
                            name={it.name}
                            favorite={it.favorite}
                            size={it.size}
                            color={it.color}
                            user={it.user}
                            data={it.data}
                            views={it.views}
                            addToFavorite={this.addToFavorite} />)
                    }
                </div>
            </div>
        )
    }
}

export default ItemListContainer;