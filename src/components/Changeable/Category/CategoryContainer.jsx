import { withRouter } from 'react-router-dom';

import CFilterContainer from './CFilter/CFilterContainer';
import ItemListContainer from './CItemList/CItemList';
import React from 'react';

import s from './Category.module.scss';

const Navigation = (props) => {
    return (
        <div className={s.navigation}>
            {props.places.map(pl => <span className={`${s.nav} t_c`}>{pl.name}</span>)}
        </div>
    )
}


class CategoryContainer extends React.Component {

    location = [{ name: 'Home' }, { name: 'Clothing' }]

    constructor({ match, ...props }) {
        super(props);
        console.log(match.params.slug);
    }

    changeLoc() {
        // this.location
    }

    render() {
        return (
            <div className={s.category_main}>
                <Navigation places={this.location} />
                <div className={s.category_inner}>
                    <CFilterContainer />
                    <ItemListContainer />
                </div>
            </div>
        )
    }
};



export default withRouter(CategoryContainer);