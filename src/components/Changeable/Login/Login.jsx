import { connect } from 'react-redux';
import { maxLengthCreator, minLengthCreator, required } from '../../../utils/validators/Validator';

import { userSignIn } from 'src/Redux/Reducers/Auth-Reducer';

import { Field, reduxForm } from 'redux-form';
import { Input } from 'src/components/Common/FormControl/FormControl';
import { Redirect } from 'react-router-dom';

import React from 'react';
import {NavLink} from 'react-router-dom';

const LoginForm = (props) => {
    const { handleSubmit } = props;
    const { minLength10 } = props;
    const { maxLength60 } = props;

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field component={Input}
                    type={'email'}
                    name={'email'}
                    validate={[required, minLength10, maxLength60]}
                    placeholder={'Enter email'} />
            </div>
            <div>
                <Field component={Input}
                    type={'password'}
                    name={'password'}
                    validate={[required, maxLength60]}
                    placeholder={'Enter password'} />
            </div>
            <div>
                <Field component={Input}
                    type={'checkbox'}
                    name={'rememberMe'}
                    placeholder={'Remember ?'} /> Remember ?
            </div>
            <div>
                <button type='submit'>Submit</button>
            </div>
        </form>
    )
};

const LoginReduxForm = reduxForm({ form: 'login', destroyOnUnmount: false })(LoginForm);


const Login = (props) => {
    const minLength10 = minLengthCreator(10);
    const maxLength60 = maxLengthCreator(30);

    const onSubmit = (formData) => {
        props.dispatch(userSignIn(formData));
    };

    return (
        <div>
            {
                props.userData.isAuth ?
                    <Redirect to={'/profile'} />
                    :
                    <div>
                        <h2>Login Page</h2>
                        <LoginReduxForm onSubmit={onSubmit} minLength10={minLength10} maxLength60={maxLength60} />
                        <NavLink to='registration'>
                            <p>Sign Up</p>
                        </NavLink>
                    </div>
            }
        </div>
    )
};

const mapStateToProps = (state) => ({ userData: state.auth });
export default connect(mapStateToProps)(Login);
