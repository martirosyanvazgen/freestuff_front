import { WithAuthRedirect } from '../../../hoc/AuthRedirect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { SignOut } from '../../../utils/components/SignOut';

import React from 'react';


const Profile = ({ userData, ...props }) => {
    return (
        <div>
            <h2>Welcome back {userData.fullName} !</h2>
            <p>{userData.email}</p>
            <p>{userData.password}</p>
            <div>
                <SignOut dispatch={props.dispatch} />
            </div>
        </div>
    )
};

const mapStateToProps = (state) => ({ userData: state.auth });
export default compose(
    connect(mapStateToProps, {}),
    WithAuthRedirect
)(Profile);
