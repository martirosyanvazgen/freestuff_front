import React from 'react';

import item_1 from 'src/assets/Images/intro/Item_1.png';
import item_2 from 'src/assets/Images/intro/Bag.png';
import item_3 from 'src/assets/Images/intro/Item_3.png';
import next from 'src/assets/Images/intro/next.png';

import s from './Introduce.module.scss';


const IMenu = (props) => {
    return (
        <div className={s.menu_list}>
            <div className={s.item}>
                <img src={item_1} alt="IMG" />
                <p className={`${s.item_text} t_c`}>Add your product, which want to exchange for another</p>
            </div>

            <div className={s.item}>
                <img src={item_2} alt="IMG" />
                <p className={`${s.item_text} t_c`}>Find products in our "Catalog</p>
            </div>

            <div className={s.item}>
                <img src={item_3} alt="IMG" />
                <p className={`${s.item_text} t_c`}>Wait for owner response</p>
            </div>
        </div>
    )
}

export const Introduce = (props) => {
    return (
        <div className={s.introduce}>
            <div className={s.intro_img}>
                <div className={s.img_block}>
                    <h2>Lorem ipsum</h2>
                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry</span>
                    <img src={next} alt="" />
                </div>
            </div>
            <IMenu />
        </div>
    )
};
