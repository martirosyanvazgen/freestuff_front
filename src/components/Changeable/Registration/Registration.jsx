import { connect } from 'react-redux';
import { maxLengthCreator, minLengthCreator, required } from 'src/utils/validators/Validator';
import { NavLink } from 'react-router-dom';
import { userSignUp } from 'src/Redux/Reducers/Auth-Reducer';

import { Field, reduxForm } from 'redux-form';
import { Input } from '../../Common/FormControl/FormControl';
import { Redirect } from 'react-router-dom';

import React from 'react';

const RegisterForm = (props) => {
    const { handleSubmit } = props;
    const { minLength10 } = props;
    const { maxLength60 } = props;

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field
                    component={Input}
                    type={'text'}
                    name={'firstName'}
                    validate={[required, maxLength60]}
                    placeholder={'Enter first name'} />
            </div>
            <div>
                <Field
                    component={Input}
                    type={'text'}
                    name={'lastName'}
                    validate={[required, maxLength60]}
                    placeholder={'Enter last name'} />
            </div>
            <div>
                <Field
                    component={Input}
                    type={'email'}
                    name={'email'}
                    validate={[required, minLength10, maxLength60]}
                    placeholder={'Enter email'} />
            </div>
            <div>
                <Field
                    component={Input}
                    type={'password'}
                    name={'password'}
                    validate={[required, maxLength60]}
                    placeholder={'Create password'} />
            </div>
            <div>
                <Field
                    component={Input}
                    type={'password'}
                    name={'confirmPassword'}
                    validate={[required, maxLength60]}
                    placeholder={'Confirm password'} />
            </div>
            <div>
                <Field
                    component={Input}
                    type={'checkbox'}
                    name={'rememberMe'}
                    placeholder={'Remember ?'} /> Remember ?
            </div>
            <div>
                <button type='submit'>Submit</button>
            </div>
        </form>
    )
};

const RegisterReduxForm = reduxForm({ form: 'register', destroyOnUnmount: false })(RegisterForm);

const Register = (props) => {
    const minLength10 = minLengthCreator(10);
    const maxLength60 = maxLengthCreator(30);

    const onSubmit = (formData) => {
        props.dispatch(userSignUp(formData));
    };

    return (
        <div>
            {
                props.userData.isAuth ?
                    <Redirect to={'/profile'} />
                    :
                    <div>
                        <h2>Registration Page</h2>
                        <RegisterReduxForm
                            onSubmit={onSubmit}
                            minLength10={minLength10}
                            maxLength60={maxLength60} />
                        <NavLink to='login'>
                            <p>Sign In</p>
                        </NavLink>
                    </div>

            }
        </div>
    )
};

const mapStateToProps = (state) => ({ userData: state.auth });
export default connect(mapStateToProps)(Register);
