import React from 'react';

import img from 'src/assets/Images/catalog.png';
import s from './Catalog.module.scss';


const Categories = (props) => {
    return (
        <div className={s.category_name}>
            <div className={s.cat_img}>
                <img src={img} alt="catIMG" />
            </div>
            <h3 className={`${s.cat_title} t_c`}>Clothing</h3>
        </div>
    )
}

const test = new Array(9).fill(undefined).map( (n, index) => <Categories key={index} />);

export const Catalog = (props) => {
    return (
        <div className={s.catalog}>
            <div className={s.heading}>
                <h2 className='t_c'>Catalog</h2>
                <p className='t_c'>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
            <div className={s.categories}>
                {test}
            </div>
        </div>
    )
}
